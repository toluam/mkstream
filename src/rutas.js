import Login from './components/login.vue'
import Principal from './components/principal.vue'
import Grupos from './components/grupos.vue'
import Camp from './components/campania.vue'
import Listas from './components/listas.vue'
import PruebaList from './components/lista.vue'


export const rutas=[{
    path:'/',component:Login
},
{ path:'/main',component:Principal
},{
    path:'/grupos',component:Grupos},
    {path:'/campania',component:Camp},
    {path:'/listas',component:Listas},
    {path:'/lista',component:PruebaList}
]