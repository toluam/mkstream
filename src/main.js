import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import {rutas} from './rutas.js'
import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'
import draggable from 'vuedraggable'
import VueCookies from 'vue-cookies'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(Datetime)
Vue.use(draggable)
Vue.use(VueCookies)
Vue.use(VueSweetalert2)




export const url="http://mkstream.com/wsMkStream/"
export var edit=new Vue()
const enrutador=new VueRouter({
  routes:rutas
})
new Vue({
  el: '#app',
  router:enrutador,
  render: h => h(App)
})
